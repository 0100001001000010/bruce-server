/*This is used solely for logging. It is not needed by the server itself, but it
  is needed for the included CLI wrapper.
*/
const fs = require("fs"),
	path = require("path");
let stream;

module.exports.startLogging = function(){
	stream = fs.createWriteStream(path.join(process.env.HOME, ".bruceserverlog"), {flags: 'a'});
};
module.exports.log = function(logText, consoleText){
	if(logText){
		stream.write((new Date().toISOString()) + " : " + logText + '\n');
	}
	if(consoleText && consoleText != "stderr"){
		console.log(logText);
	} else if(consoleText == "stderr") {
		console.warn(logText);
	}
};
module.exports.stopLogging = function(){
	stream.end();
};
